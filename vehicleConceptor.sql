CREATE DATABASE IF NOT EXISTS `vehicle_conceptor`;
DROP TABLE IF EXISTS `car`;

DROP TABLE IF EXISTS `bike`;

DROP TABLE IF EXISTS `bicycle`;

DROP TABLE IF EXISTS `motorVehicle`;

DROP TABLE IF EXISTS `vehicle`;

CREATE TABLE vehicle (
    id_vehicle INTEGER PRIMARY KEY AUTO_INCREMENT,
    brand VARCHAR(50) NOT NULL,
    color VARCHAR(50) NOT NULL,
    dateOfPurchase DATE NOT NULL,
    purchasePrice DOUBLE NOT NULL,
    sellingPrice DOUBLE NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE bicycle (
    id_bicycle INTEGER PRIMARY KEY,
    bicycleType VARCHAR(50),
    numberOfTrail INT NOT NULL,
    isMarked BOOLEAN,
    FOREIGN KEY (id_bicycle) REFERENCES vehicle(id_vehicle) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE motorVehicle (
    id_motorVehicle INTEGER PRIMARY KEY,
    mileage INT NOT NULL,
    dateOfTheFirstCirculation DATETIME,
    FOREIGN KEY (id_motorVehicle) REFERENCES vehicle(id_vehicle) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE car (
    carId INTEGER PRIMARY KEY,
    carType VARCHAR(50),
    numberOfDoors INT NOT NULL,
    automaticGearBox BOOLEAN,
    horsePower INT NOT NULL,
    FOREIGN KEY (carId) REFERENCES motorVehicle(id_motorVehicle) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE bike (
    id_bike INTEGER PRIMARY KEY,
    bikeType VARCHAR(50),
    engine INT NOT NULL,
    horsePower INT NOT NULL,
    FOREIGN KEY (id_bike) REFERENCES motorVehicle(id_motorVehicle) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO
    vehicle (
        brand,
        color,
        dateOfPurchase,
        purchasePrice,
        sellingPrice
    )
VALUES
    ('peugeot', "bleu", "12/12/12", 750, 1250),
    ('mercedes', "blanc", "12/12/12", 750, 1250),
    ('renaud', "violine", "12/12/12", 750, 1250),
    ('audi', "rouge", "12/12/12", 750, 1250),
    ('citroen', "vert", "12/12/12", 750, 1250),
    ('peugeot', "bleu", "12/12/12", 750, 1250),
    ('mercedes', "blanc", "12/12/12", 750, 1250),
    ('renaud', "violine", "12/12/12", 750, 1250),
    ('audi', "rouge", "12/12/12", 750, 1250),
    ('citroen', "vert", "12/12/12", 750, 1250),
    ('peugeot', "bleu", "12/12/12", 750, 1250),
    ('mercedes', "blanc", "12/12/12", 750, 1250),
    ('renaud', "violine", "12/12/12", 750, 1250),
    ('audi', "rouge", "12/12/12", 750, 1250),
    ('citroen', "vert", "12/12/12", 750, 1250);

INSERT INTO
    motorVehicle (
        id_motorVehicle,
        mileage,
        dateOfTheFirstCirculation
    )
VALUES
    (1, 42123, "12/12/12"),
    (2, 17134, "12/12/12"),
    (3, 33214, "12/12/12"),
    (4, 22342, "12/12/12"),
    (5, 11234, "12/12/12"),
    (6, 42123, "12/12/12"),
    (7, 17134, "12/12/12"),
    (8, 33214, "12/12/12"),
    (9, 22342, "12/12/12"),
    (10, 11234, "12/12/12");

INSERT INTO
    bicycle (id_bicycle, bicycleType, numberOfTrail, isMarked)
VALUES
    (11, 'regular', 27, 0),
    (12, 'regular', 14, 1),
    (13, 'regular', 5, 0),
    (14, 'Electrique', 14, 1),
    (15, 'Electrique', 21, 0);

INSERT INTO
    car (
        carId,
        carType,
        numberOfDoors,
        automaticGearBox,
        horsePower
    )
VALUES
    (1, 'Essence', 5, true, 75),
    (2, 'Essence', 3, false, 125),
    (3, 'Diesel', 5, true, 75),
    (4, 'Disel', 3, false, 150),
    (5, 'Electrique', 3, true, 70);

INSERT INTO
    bike (id_bike, bikeType, engine, horsePower)
VALUES
    (6, 'Essence', 50, 75),
    (7, 'Essence', 600, 1000),
    (8, 'Essence', 750, 1250),
    (9, 'Electrique', 125, 150),
    (10, 'Electrique', 75, 270);