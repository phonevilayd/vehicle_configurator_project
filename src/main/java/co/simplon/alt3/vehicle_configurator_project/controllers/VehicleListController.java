package co.simplon.alt3.vehicle_configurator_project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.alt3.vehicle_configurator_project.repositories.VehicleRepositoryJDBC;

@Controller
public class VehicleListController {
    
    @Autowired
    VehicleRepositoryJDBC vehicleRepo;

    @GetMapping("/vehicles")
    public String showVehicles(Model model) {
        model.addAttribute("vehicles", vehicleRepo.findAll());
        return "vehicles";
    }
}
