package co.simplon.alt3.vehicle_configurator_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleConfiguratorProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleConfiguratorProjectApplication.class, args);
	}

}
