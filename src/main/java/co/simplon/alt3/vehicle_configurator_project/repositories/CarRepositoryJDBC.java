package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator_project.entities.Car;

@Repository
public class CarRepositoryJDBC implements IRepository<Car> {

    @Override
    public List<Car> findAll() {
        List<Car> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM car");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToCar(rs));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Car findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM car WHERE carId=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToCar(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Car sqlToCar(ResultSet rs) throws SQLException {
        return new Car(
            rs.getInt("carId"),
            rs.getString("carType"),
            rs.getInt("numberOfDoors"),
            rs.getBoolean("automaticGearBox"),
            rs.getInt("horsePower")
            );

    }
    
}
