package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator_project.entities.Vehicle;

@Repository
public class VehicleRepositoryJDBC implements IRepository<Vehicle> {

    @Override
    public List<Vehicle> findAll() {
        List<Vehicle> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM vehicle");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToVehicle(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Vehicle findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM vehicle WHERE id_vehicle=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToVehicle(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Vehicle sqlToVehicle(ResultSet rs) throws SQLException {
        return new Vehicle(
            rs.getInt("id_vehicle"),
            rs.getString("brand"),
            rs.getString("color"),
            rs.getDate("dateOfPurchase"),
            rs.getDouble("purchasePrice"),
            rs.getDouble("sellingPrice")

        );
    }
    
}
