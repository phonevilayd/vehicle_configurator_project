package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator_project.entities.Bicycle;

@Repository
public class BicycleRepositoryJDBC implements IRepository<Bicycle> {

    @Override
    public List<Bicycle> findAll() {
        List<Bicycle> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM bicycle");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToBicycle(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Bicycle findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM bicycle WHERE id_bicycle=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToBicycle(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bicycle sqlToBicycle(ResultSet rs) throws SQLException {
        return new Bicycle(
            rs.getInt("id_bicycle"),
            rs.getString("bicycleType"),
            rs.getInt("numberOfTrail"),
            rs.getBoolean("isMarked")
        );
    }
    
}
