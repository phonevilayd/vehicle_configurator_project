package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator_project.entities.Bike;

@Repository
public class BikeRepositoryJDBC implements IRepository<Bike> {

    @Override
    public List<Bike> findAll() {
        List<Bike> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM bike");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToBike(rs));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Bike findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM bike WHERE id_bike=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToBike(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bike sqlToBike(ResultSet rs) throws SQLException {
        return new Bike(
            rs.getInt("id_bike"),
            rs.getString("bikeType"),
            rs.getInt("engine"),
            rs.getInt("horsePower")
            );

    }
    
}
