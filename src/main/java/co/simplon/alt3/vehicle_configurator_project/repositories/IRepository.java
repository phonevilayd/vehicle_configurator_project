package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    T findById(int id);
}
