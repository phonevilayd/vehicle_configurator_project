package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator_project.entities.MotorVehicle;

@Repository
public class MotorVehicleRepositoryJDBC implements IRepository<MotorVehicle>{

    @Override
    public List<MotorVehicle> findAll() {
        List<MotorVehicle> list = new ArrayList<>();
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM motorVehicle");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToMotorVehicle(rs));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return list;
    }

    @Override
    public MotorVehicle findById(int id) {
        try (Connection connection = DbConnect.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM motorVehicle WHERE id_motorvehicle=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToMotorVehicle(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }

    private MotorVehicle sqlToMotorVehicle(ResultSet rs) throws SQLException {
        return new MotorVehicle(
            rs.getInt("id_motorvehicle"),
            rs.getInt("mileage"),
            rs.getDate("dateOfTheFirstCirculation")
            );

    }
    
}
