package co.simplon.alt3.vehicle_configurator_project.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnect {
    
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/vehicle_conceptor");
    }
}
