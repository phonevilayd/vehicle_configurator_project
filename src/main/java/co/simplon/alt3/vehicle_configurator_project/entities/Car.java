package co.simplon.alt3.vehicle_configurator_project.entities;

public class Car extends MotorVehicle {
    private Integer carId;
    private String carType;
    private int numberOfDoors;
    private boolean automaticGearBox;
    private int horsePower;
    
    public Car(String carType, int numberOfDoors, boolean automaticGearBox, int horsePower) {
        this.carType = carType;
        this.numberOfDoors = numberOfDoors;
        this.automaticGearBox = automaticGearBox;
        this.horsePower = horsePower;
    }

    public Car(Integer carId, String carType, int numberOfDoors, boolean automaticGearBox, int horsePower) {
        this.carId = carId;
        this.carType = carType;
        this.numberOfDoors = numberOfDoors;
        this.automaticGearBox = automaticGearBox;
        this.horsePower = horsePower;
    }

    public Car() {
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public boolean isAutomaticGearBox() {
        return automaticGearBox;
    }

    public void setAutomaticGearBox(boolean automaticGearBox) {
        this.automaticGearBox = automaticGearBox;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }
}
