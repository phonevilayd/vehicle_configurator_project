package co.simplon.alt3.vehicle_configurator_project.entities;

import java.util.Date;

public class MotorVehicle extends Vehicle {
  private Integer id_motorvehicle;
  private int mileage;
  private Date dateOfTheFirstCirculation;

  public MotorVehicle(int mileage, Date dateOfTheFirstirculation) {
    this.mileage = mileage;
    this.dateOfTheFirstCirculation = dateOfTheFirstirculation;
}

public MotorVehicle(Integer id_motorvehicle, int mileage, Date dateOfTheFirstirculation) {
    this.id_motorvehicle = id_motorvehicle;
    this.mileage = mileage;
    this.dateOfTheFirstCirculation = dateOfTheFirstirculation;
}

public MotorVehicle() {
}

public Integer getId_motorvehicle() {
    return id_motorvehicle;
}

public void setId_motorvehicle(Integer id_motorvehicle) {
    this.id_motorvehicle = id_motorvehicle;
}

public int getMileage() {
    return mileage;
}

public void setMileage(int mileage) {
    this.mileage = mileage;
}

public Date getDateOfTheFirstirculation() {
    return dateOfTheFirstCirculation;
}

public void setDateOfTheFirstirculation(Date dateOfTheFirstirculation) {
    this.dateOfTheFirstCirculation = dateOfTheFirstirculation;
}  
}
