package co.simplon.alt3.vehicle_configurator_project.entities;

public class Bicycle extends Vehicle {
    private Integer id_bicycle;
    private String bicycleType;
    private int numberOfTrail;
    private boolean isMarked;
   
    public Bicycle(String bikeType, int numberOfTrail, boolean isMarked) {
        this.bicycleType = bikeType;
        this.numberOfTrail = numberOfTrail;
        this.isMarked = isMarked;
    }

    public Bicycle() {
    }

    public Bicycle(Integer id_bicycle, String bikeType, int numberOfTrail, boolean isMarked) {
        this.id_bicycle = id_bicycle;
        this.bicycleType = bikeType;
        this.numberOfTrail = numberOfTrail;
        this.isMarked = isMarked;
    }

    public Integer getId_bicycle() {
        return id_bicycle;
    }

    public void setId_bicycle(Integer id_bicycle) {
        this.id_bicycle = id_bicycle;
    }

    public String getBikeType() {
        return bicycleType;
    }

    public void setBikeType(String bikeType) {
        this.bicycleType = bikeType;
    }

    public int getNumberOfTrail() {
        return numberOfTrail;
    }

    public void setNumberOfTrail(int numberOfTrail) {
        this.numberOfTrail = numberOfTrail;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public void setMarked(boolean isMarked) {
        this.isMarked = isMarked;
    }
    
}
